import numpy as np
from .numtheory import sieve, powerlist


_MAX_PRIME = 31
#_primes = sieve(_MAX_PRIME)

_base_fractions = {
        'C' : (1,1),
        'D' : (9,8),
        'E' : (5,4),
        'F' : (4,3),
        'G' : (3,2),
        'A' : (5,3),
        'B' : (15,8)
        }

_accidental_fractions = {
        'plus'  : (81,80),
        'sharp' : (25,24),
        7  : (35,36),
        11 : (33,32),
        13 : (65,64),
        17 : (51,50),
        19 : (95,96),
        23 : (46,45),
        29 : (145,144),
        31 : (31,30)
        }

_positive_accidental_names = {
        'plus'  : '+',
        'sharp' : '#',
        7  : '7',
        11 : '^',
        13 : ' 13',
        17 : ' 17',
        19 : ' 19',
        23 : ' 23',
        29 : ' 29',
        31 : ' 31'
        }

_negative_accidental_names = {
        'plus'  : '-',
        'sharp' : 'b',
        7  : 'L',
        11 : 'v',
        13 : ' El',
        17 : ' Ll',
        19 : ' 6l',
        23 : ' E5',
        29 : ' 65',
        31 : ' lE'
        }


def note_name(p,q):
    ''' Returns a note name within Ben Johnston's notation system given p, q
    corresponding to a fractional representation p/q of a tone.
    p and q do not have to be coprime, but neither can contain prime factors
    larger than _MAX_PRIME as defined above.

    Future change note: may have it return octave number too.
    '''
    note = ""

    base, *nn_acc = decompose(p,q)

    letter_name = { tuple(powerlist(*v)[1:3]): k for k, v in _base_fractions.items() }

    # This will only work in Python 3.6 and beyond
    pos_acc_name = list(_positive_accidental_names.values())
    neg_acc_name = list(_negative_accidental_names.values())

    letter = letter_name[(base[1],base[2])]
    note += letter

    # TODO: determine octave number.

    for n in range(len(nn_acc)):
        n_acc = nn_acc[n]
        if(n_acc > 0):
            note += n_acc*pos_acc_name[n]
        if(n_acc < 0):
            note += (-n_acc)*neg_acc_name[n]

    return note


def fraction(name):
    ''' Returns numbers p, q in a fractional representationl p/q given a note
    name in Ben Johnston's notation system.
    '''
    # TODO
    return


################################################################################


def acc_to_powerlist(key):
    ''' Returns an accidental as a powerlist. Converts to a numpy array
    so that addition works as expected, i.e., by element values.
    '''
    fraction = _accidental_fractions[key]
    plist = powerlist(*fraction)
    return np.array(plist)


################################################################################


def decompose_5limit(target):
    ''' Decomposes a powerlist representation of a 5-limit tone into a base tone
    and a collection of +/- and #/b accidentals.
    '''
    n_sharp = 0
    n_plus = 0

    # Powerlist representations of accidentals we'll be using.
    # migr5 migrates us along the 5-axis.
    sharp = acc_to_powerlist('sharp')
    plus  = acc_to_powerlist('plus')
    migr5 = 2*plus + sharp

    # Step one: move the target to contain no powers of 5
    while(target[2] > 0):
        n_plus -= 1
        target += plus
    while(target[2] < 0):
        n_plus += 1
        target -= plus

    # Step two: move the target so that powers of 3 fall within [-1,5]
    while(target[1] < -1):
        target += migr5
        n_plus -= 2
        n_sharp -= 1
    while(target[1] > 5):
        target -= migr5
        n_plus += 2
        n_sharp += 1

    # Step 3: if the power of 3 is within [3,5], move it down by (-4,1)
    if(target[1] > 2):
        target -= plus
        n_plus += 1

    base = list(target)
    return base, n_plus, n_sharp


def decompose_powerlist(target):
    ''' Decomposes a powerlist representation of a tone into a base tone and a
    collection of accidentals. Works recursively until a 5-limit target remains.
    '''
    all_primes = sieve(_MAX_PRIME)
    prime_limit = all_primes[len(target)-1]

    # A list of 2-3 or less elements means only primes 2, 3, and 5 remain.
    if(len(target) < 4):
        return decompose_5limit(target)

    acc_op = acc_to_powerlist(prime_limit)
    n_acc = target[-1] // acc_op[-1]
    target -= acc_op*n_acc

    smol_target = target[0:-1]
    smol_list = decompose_powerlist(smol_target)
    return smol_list+(n_acc,)


def decompose(p,q):
    ''' Decomposes a fractional representation of a tone, p/q, into a base
    tone and a collection of accidentals. A purely numerical representation
    is returned by this routine:
    - A list of three numbers representing the powers of 2, 3, and 5 in the
      base tone.
    - Integers representing the numbers of plusses (syntonic commas), sharps,
      and then prime number accidentals that must be applied to the base tone.
    '''
    target = np.array( powerlist(p,q) )
    return decompose_powerlist(target)
