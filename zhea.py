import numpy as np
import math


def cents(x):
    ''' Find cent value of interval x. '''
    return 1200*np.log(x)/np.log(2)


def approx_in_edo(x, nEDO):
    ''' Find closest approximation to interval x in nEDO.
    Returns the number of EDO steps that best approximates x,
    along with the error in cents.
    '''
    cx = cents(x)
    xsteps = nEDO*cx/1200
    nsteps = round(xsteps)
    cn = nsteps/nEDO*1200
    error = cn-cx
    return nsteps, error

def approx_in_mos(x, npers, gen, notes_per_period, gens_up):
    ''' Find closest approximation to interval x in a given mode of the MOS scale pattern 
	with period 1\npers and generator size gen in cents, 
	with number of generators stacked == notes_per_period - 1, and
	with the mode parametrized by gens_up (0 <= gens_up < npers*notes_per_period).
	Returns the interval from the tonic that best approximates x, 
	along with the error in cents.
    '''
    if(not(0 <= gens_up < notes_per_period)):
        raise ValueError("Must have 0 <= gens_up < notes_per_period.")
	
    cx = cents(x)
    the_mode = generate_mos_mode(npers, gen, notes_per_period, gens_up)
    octaves_up = cx//1200
    cx_reduced = math.fmod(cx,1200)
    scale_size = notes_per_period*npers
    i = 0
    while(i < scale_size and the_mode[i] < cx_reduced):
        i += 1
    bigger = the_mode[i] # the first note bigger than cx_reduced
    smaller = the_mode[i-1] # the last note smaller than cx_reduced
    best_approx = 0
    error = 0
    if abs(cx_reduced - bigger) < abs(cx_reduced - smaller):
        error = bigger - cx_reduced
        best_approx = 1200*octaves_up + bigger
    else:
        error = smaller - cx_reduced
        best_approx = 1200*octaves_up + smaller
    return best_approx, error, the_mode

def overtone_approx_in_edo(a, b, nEDO, **kwargs):
    ''' For a<b, find the best approximation of the lineal segment
    a:a+1:...:b-1:b within nEDO.
    Returns a list of pairs, each pair containing the number of edo steps
    and the error for each tone in the segment (with the segment starting
    from 0).
    '''
    if(a>=b):
        raise ValueError("Must have a < b.")

    max_error = kwargs.get('max_error', np.inf)
    
    harmonics = []
    steps = []
    tones = []
    net_error = 0
    for nharmonic in range(a,b+1):
        x = nharmonic/a
        nsteps, error = approx_in_edo(x,nEDO)
        if(abs(error) < max_error):
            harmonics += [nharmonic]
            steps += [nsteps]
            tones += [(nharmonic,nsteps,error)]
            net_error += abs(error)

    info = {
            'harmonics' : harmonics,
            'steps' : steps,
            'tones' : tones,
            'net_error' : net_error
            }
    return info

def overtone_approx_in_mos(a, b, npers, gen, notes_per_period, gens_up, **kwargs):
    ''' For a<b, find the best approximation of the lineal segment
    a:a+1:...:b-1:b within a given mode of the rank-2 scale with generator gen and period 1\npers.
    Returns a list of pairs, each pair containing the number of edo steps
    and the error for each tone in the segment (with the segment starting
    from 0).
    '''
    if(not(0 <= gens_up < notes_per_period)):
        raise ValueError("Must have 0 <= gens_up < notes_per_period.")
    max_error = kwargs.get('max_error', np.inf)
    in_mos_mode = []
    harmonics = []
    cents = []
    tones = []
    net_error = 0
	
    for nharmonic in range(a,b+1):
        x = nharmonic/a
        ncents, error, in_mos_mode = approx_in_mos(x, npers, gen, notes_per_period, gens_up)
        if(abs(error) < max_error):
            harmonics += [nharmonic]
            cents += [ncents]
            tones += [(nharmonic,ncents,error)]
            net_error += abs(error)
    info = {
            'harmonics' : harmonics,
            'cents' : cents,
            'tones' : tones,
            'net_error' : net_error
            }
    return in_mos_mode, info
	

def find_overtone_chords_in_edo(a_max, b_max, nEDO, **kwargs):
    ''' Construct a list of all valid overtone chords of nEDO.
    By default, I use a max_error of 7 cents (for now).
    This just returns all the overtone modes as a list.
    '''
    if(a_max>=b_max):
        raise ValueError("Must have a_max < b_max.")

    max_error = kwargs.get('max_error', 7)
    a_min = kwargs.get('a_min', 1)

    modes = []
    for a in range(a_min,a_max+1):
        mode_a = overtone_approx_in_edo(a, b_max, nEDO, max_error=max_error)
        modes += [mode_a]
    return modes
	
def find_overtone_chords_in_mos(a_max, b_max, npers, gen, notes_per_period, gens_up, **kwargs):
    ''' Construct a list of all valid overtone chords of a specific mode of a rank-2 scale 
	with period 1\npers and generator gen, 
	while also labeling the corresponding mode of the rank-2 scale.
    '''
    if(a_max>=b_max):
        raise ValueError("Must have a_max < b_max.")

    max_error = kwargs.get('max_error', 7)
    a_min = kwargs.get('a_min', 1)

    scale_size = npers*notes_per_period
    modes = []
    for a in range(a_min,a_max+1):
        mode_a = overtone_approx_in_mos(a, b_max, npers, gen, notes_per_period, gens_up, max_error=max_error)[1]
        modes += [mode_a]
    info = {
            'in_mos_mode' : generate_mos_mode(npers, gen, notes_per_period, gens_up),
            'modes': modes
            }
    return info
	
def generate_mos_mode(npers, gen, notes_per_period, gens_up):
    '''Returns an array of cent values (from the tonic) 
    for a mode of the given MOS pattern and the given rotation.
    gens_up parametrizes number of generators up relative to
    the total number of generators stacked.'''
    if(not(0 <= gens_up < notes_per_period)):
        raise ValueError("Must have 0 <= gens_up < notes_per_period.")

    mode = []
    # Get the pattern within each period: stack the generator from tonic notes_per_period - 1 times, take the cent value mod period.
    pattern = []
    per = 1200/npers
    for i in range(0, notes_per_period):
        next_gen = gen*(i+1-notes_per_period+gens_up)
        if next_gen > 0:
            newnote = math.fmod(next_gen, per)
        else:
            newnote = math.fmod(next_gen, per) + per
        pattern += [newnote]
    
    # sort pattern
    pattern.sort()

    
    # The array pattern starts with the first interval and ends with the period.
    # Stack the above pattern npers times 
    for i in range(npers):
        base = 1200/npers*i
        for j in range(len(pattern)):
            mode += [base+pattern[j]]

    # Reduce
    for i in range(len(mode)):
        if mode[i] >= 0:
            mode[i] = math.fmod(mode[i], 1200)
        else:
            mode[i] = math.fmod(mode[i]+1200, 1200)
    mode.sort()
    mode += [1200.]
    return mode
