def sieve(n):
    ''' Gets all prime numbers less than or equal to n. '''
    primes = []
    others = []
    for i in range(2,n+1):
        if i not in others:
            primes += [i]
            others += [i*j for j in range(2,n//i+1)]
    return primes


def factorize(n):
    ''' Returns a prime factorization of n. '''
    prime_list = sieve(n)
    factors = []
    for p in prime_list:
        while(n%p == 0):
            factors += [p]
            n /= p
    return factors


def isPrime(n):
    ''' Returns true iff the number is prime. '''
    plist = sieve(n)
    if(n==plist[-1]):
        return True
    return False


def highest_prime(n):
    ''' Return the highest prime factor of n. '''
    factors = factorize(n)
    return max(factors)


def powerlist(p,q):
    ''' Returns a list of powers of prime numbers for p/q, where p and q are
    integers. It does not matter if p and q are coprime.
    '''
    pfacs = factorize(p)
    qfacs = factorize(q)
    try:
        max_prime = max(pfacs+qfacs)
    except ValueError:
        max_prime = 0
    prime_list = sieve(max_prime)
    powers = [ pfacs.count(prime) - qfacs.count(prime) for prime in prime_list ]
    # Must have at least 3 elements for other functions to work.
    while(len(powers) < 3):
        powers += [0]
    return powers


def invert(n,p):
    ''' Find the inverse of n in Z_p. '''
    b = n
    r = b%p
    while(r!=1):
        b += n
        r = b%p
    return b//n

