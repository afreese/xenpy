These are just some silly scripts I'm using to help with
the composition of Xenharmonic music.
I make no promise that they're useful for anyone else,
or that they're complete in any way.
But I'm sharing them in case they might be helpful for someone.

## Requirements
numpy and matplotlib are needed for this to work.
