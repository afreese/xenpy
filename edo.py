import numpy as np
from .numtheory import sieve, isPrime

# Matplotlib initialization
import matplotlib as mpl
mpl.use('Agg')
mpl.rc('font',size=20,family='cmr10',weight='normal')
mpl.rc('text',usetex=True)
from matplotlib import pyplot as plt


################################################################################
# Some numerical stuff

def cents(x):
    ''' Find cent value of interval x. '''
    return 1200*np.log(x)/np.log(2)


def approx(x, nEDO):
    ''' Find closest approximation to interval x in nEDO.
    Returns the number of EDO steps that best approximates x,
    along with the error in cents.
    '''
    cx = cents(x)
    xsteps = nEDO*cx/1200
    nsteps = round(xsteps)
    cn = nsteps/nEDO*1200
    error = cn-cx
    return nsteps, error


def overtone_approx(a, b, nEDO, **kwargs):
    ''' For a<b, find the best approximation of the lineal segment
    a:a+1:...:b-1:b within nEDO.
    Returns a list of pairs, each pair containing the number of edo steps
    and the error for each tone in the segment (with the segment starting
    from 0).
    '''
    if(a>=b):
        raise ValueError("Must have a < b.")

    max_error = kwargs.get('max_error', np.inf)

    harmonics = []
    steps = []
    tones = []
    net_error = 0
    for nharmonic in range(a,b+1):
        x = nharmonic/a
        nsteps, error = approx(x,nEDO)
        if(abs(error) < max_error):
            harmonics += [nharmonic]
            steps += [nsteps]
            tones += [(nharmonic,nsteps,error)]
            net_error += abs(error)

    info = {
            'harmonics' : harmonics,
            'steps' : steps,
            'tones' : tones,
            'net_error' : net_error
            }
    return info


def find_overtone_modes(a_max, b_max, nEDO, **kwargs):
    ''' Construct a list of all valid overtone modes of nEDO.
    By default, I use a max_error of 7 cents (for now).
    This just returns all the overtone modes as a list.
    '''
    if(a_max>=b_max):
        raise ValueError("Must have a_max < b_max.")

    max_error = kwargs.get('max_error', 7)
    a_min = kwargs.get('a_min', 1)

    modes = []
    for a in range(a_min,a_max+1):
        mode_a = overtone_approx(a, b_max, nEDO, max_error=max_error)
        modes += [mode_a]

    return modes


################################################################################
# EDO ploter stuff

def edoNoteNames(nEDO):
    # TODO
    # 17 magic list for now
    if(nEDO==1):
        return [ "C" ]
    elif(nEDO==2):
        return [ "C", "G" ]
    elif(nEDO==3):
        return [ "C", "E", "G" ]
    elif(nEDO==4):
        return [ "C", "E", "G", "A" ]
    elif(nEDO==5):
        return [ "C", "D", "E", "G", "A" ]
    elif(nEDO==6):
        return [ "C", "D", "E", "F", "G", "A" ]
    elif(nEDO==7):
        return [ "C", "D", "E", "F", "G", "A", "B" ]
    elif(nEDO==8):
        return [ "C", "D", "E", r"F\#", r"G\#", r"A\#", r"B\#", "Bb" ]
    # TODO: 9-11
    elif(nEDO==12):
        return [
                "C", r"C\#", "D", "Eb", "E", "F", r"F\#",
                "G", "Ab", "A", "Bb", "B"
                ]
    # TODO: 13-16
    elif(nEDO==17):
        return [
                "C", "Db", r"C\#", "D", "Eb", r"D\#", "E", "F",
                "Gb", r"F\#", "G", "Ab", r"G\#", "A", "Bb", r"A\#", "B"
                ]
    # TODO: 18
    elif(nEDO==19):
        return [
                "C", r"C\#", "Db", "D", r"D\#", r"Eb", "E", "Fb",
                "F", r"F\#", "Gb", "G", r"G\#", "Ab", "A", r"A\#", "Bb", "B", r"B\#"
                ]
    # TODO: 20+
    else:
        raise NotImplementedError("Haven't implemented {:d}EDO yet.".format(nEDO))


class edoPlotter():
    def __init__(self, **kwargs):
        '''Constructor'''
        # Optional arguments
        self.edo = kwargs.get('edo', 12)
        self.ext = kwargs.get('ext','pdf')
        # Prepare canvas
        self.figure = plt.figure( figsize=(6,6) )
        self.canvas = self.figure.add_subplot(1,1,1, polar=True)
        self.canvas.set_theta_direction(-1) # clockwise
        self.canvas.set_theta_offset(np.pi/2.0) # tonic at top ... TODO: generalize!
        self.canvas.set_xticks(np.linspace(0, 2*np.pi*(1-1/self.edo), self.edo))
        self.canvas.set_xticklabels(edoNoteNames(self.edo))
        self.canvas.set_yticks([])
        return

    def __del__(self):
        '''Garbage collection'''
        plt.close(self.figure)
        return

    def plotLine(self, centval, **kwargs):
        xval = centval/1200*2*np.pi
        ymin = 0
        ymax = 1
        color = kwargs.get('color', 'k')
        line  = kwargs.get('line', '-')
        self.canvas.vlines(xval, ymin, ymax, linewidth = 2,
                color = color, linestyle = line)
        if('label' in kwargs):
            text = kwargs['label']
            xloc = xval + 0.125
            yloc = 0.85
            self.canvas.annotate(text, xy=(xloc,yloc), color=color,
                    horizontalalignment = 'center', verticalalignment = 'center')
        return

    def save(self, filebase):
        '''Save the figure'''
        self.canvas.set_ylim((0,1))
        self.figure.tight_layout()
        filename = "{}.{}".format(filebase,self.ext)
        if(self.ext=='png'):
            self.figure.savefig(filename, dpi=250)
        else:
            self.figure.savefig(filename)
        print("Plot saved to {}".format(filename))
        return


def plotHarmonics(*harmonics, **kwargs):
    edo = kwargs.get('edo', 12)
    ext = kwargs.get('ext', 'pdf')
    filename = kwargs.get('filename', 'harmonics{:d}'.format(edo))

    fig = edoPlotter(edo=edo, ext=ext)

    # colors
    blue = '#377eb8'
    orange = '#ff7f00'
    green = '#4daf4a'
    red = '#e41a1c'
    pink = '#f781bf'

    # Error tolerance for coloring
    edostep = 1200/edo
    tight_err_tol = 5
    loose_err_tol = edostep/4

    for n in harmonics:
        _, err = approx(n, edo)
        if(isPrime(n)):
            line = '-'
        else:
            line = '--'
        if(abs(err) < tight_err_tol):
            color = blue
        elif(abs(err) < loose_err_tol):
            color = green
        else:
            color = pink

        fig.plotLine(cents(n), label=str(n), color=color, line=line)

    fig.save(filename)
    return


usefulHarmonics = [2,3,5,7,9,11,13,15,17,19,21,23,25]
